"""
Utilizando a função feita no arquivo Q2.py, escreva um programa que vá pedindo sucessivamente
aos jogadores 1 e 2 para introduzirem as jogadas respectivas, anunciando
de seguida o resultado do jogo.
O seu programa deve terminar se algum dos jogadores introduzir uma jogada inválida.

Exemplo do programa rodando

>>> Jogador 1, por favor introduza a sua jogada: pedra
>>> Jogador 2, por favor introduza a sua jogada: papel
>>> Parabéns, jogador 2, ganhou o jogo!
>>> Jogador 1, por favor introduza a sua jogada: tesoura
>>> Jogador 2, por favor introduza a sua jogada: papel
>>> Parabéns, jogador 1, ganhou o jogo!
>>> Jogador 1, por favor introduza a sua jogada: wejkrlw
>>> Jogador 2, por favor introduza a sua jogada: tesoura
>>> Jogo terminado.
"""
