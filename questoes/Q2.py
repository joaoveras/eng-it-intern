'''
Pedra-papel-tesoura é um jogo envolvendo dois jogadores em que cada jogador escolhe uma de 3
jogadas possíveis: “pedra”, “papel” ou “tesoura”. O resultado do jogo é determinado com base
nas seguintes regras:
  - Um jogador que escolha “pedra” perde o jogo se o outro escolher “papel” e ganha
  caso o outro escolha “tesoura”.
  - Um jogador que escolha “papel” perde o jogo se o outro escolher “tesoura” e ganha
  caso o outro escolha “pedra”.
  - Um jogador que escolha “tesoura” perde o jogo se o outro escolher “pedra” e ganha
  caso o outro escolha “papel”.

No caso de ambos escolhem a mesma jogada, o jogo é considerado um empate

Escreva uma função com o nome pedra_papel_tesoura que recebe como argumentos duas cadeias
de caracteres, correspondendo às jogadas dos dois jogadores, e anuncia qual dos dois ganhou,
escrevendo uma mensagem. A sua função tem o valor lógico False se algum dos argumentos
for inválido (isto é, não corresponder a uma das 3 cadeias de caracteres ’pedra’, ’papel’ ou ’tesoura’)
e True caso contrário.

Ex.:
>>> pedra_papel_tesoura('pedra', 'papel')
Parabens, jogador 2, ganhou o jogo!
True
>>> pedra_papel_tesoura('tesoura', 'papel')
Parabens, jogador 1, ganhou o jogo!
True
>>> pedra_papel_tesoura('papel', 'papel')
O jogo foi um empate.
True
>>> pedra_papel_tesoura('papel', 'jhg')
False

'''

#start from here
